<?php
/**
 * Created by PhpStorm.
 * User: Acer
 * Date: 17/04/2018
 * Time: 22.56
 */

namespace common\widgets;


use wbraganca\dynamicform\DynamicFormAsset;

class DynamicFormAssetBagiData extends DynamicFormAsset
{
    public function init()
    {
        $this->setSourcePath(__DIR__ . '/assets');
        $this->setupAssets('js', ['yii2-dynamic-form']);
        parent::init();
    }
}