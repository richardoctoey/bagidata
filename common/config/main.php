<?php
return [
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        'authManager' => [
            'class' => 'yii\rbac\DbManager'
        ],
        'user' => [
            'loginUrl' => ['user/security/login']
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
    ],
    'modules' => [
        'user' => [
            'class'  => 'dektrium\user\Module',
            'modelMap' => [
                'User' => 'backend\models\User',
                'Profile' => 'backend\models\ProfileBagiData'
            ],
            'enableConfirmation'=>True,
        ],
        'admin' => [
            'class' => 'mdm\admin\Module',
        ]
    ]
];
