-- MySQL dump 10.16  Distrib 10.1.31-MariaDB, for Win32 (AMD64)
--
-- Host: 127.0.0.1    Database: yii2advanced
-- ------------------------------------------------------
-- Server version	10.1.31-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `auth_assignment`
--

DROP TABLE IF EXISTS `auth_assignment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_assignment` (
  `item_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`item_name`,`user_id`),
  KEY `auth_assignment_user_id_idx` (`user_id`),
  CONSTRAINT `auth_assignment_ibfk_1` FOREIGN KEY (`item_name`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_assignment`
--

LOCK TABLES `auth_assignment` WRITE;
/*!40000 ALTER TABLE `auth_assignment` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_assignment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_item`
--

DROP TABLE IF EXISTS `auth_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_item` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `type` smallint(6) NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `rule_name` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data` blob,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`name`),
  KEY `rule_name` (`rule_name`),
  KEY `idx-auth_item-type` (`type`),
  CONSTRAINT `auth_item_ibfk_1` FOREIGN KEY (`rule_name`) REFERENCES `auth_rule` (`name`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_item`
--

LOCK TABLES `auth_item` WRITE;
/*!40000 ALTER TABLE `auth_item` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_item_child`
--

DROP TABLE IF EXISTS `auth_item_child`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_item_child` (
  `parent` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `child` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`parent`,`child`),
  KEY `child` (`child`),
  CONSTRAINT `auth_item_child_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `auth_item_child_ibfk_2` FOREIGN KEY (`child`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_item_child`
--

LOCK TABLES `auth_item_child` WRITE;
/*!40000 ALTER TABLE `auth_item_child` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_item_child` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_rule`
--

DROP TABLE IF EXISTS `auth_rule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_rule` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `data` blob,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_rule`
--

LOCK TABLES `auth_rule` WRITE;
/*!40000 ALTER TABLE `auth_rule` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_rule` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bank`
--

DROP TABLE IF EXISTS `bank`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bank` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bank`
--

LOCK TABLES `bank` WRITE;
/*!40000 ALTER TABLE `bank` DISABLE KEYS */;
INSERT INTO `bank` VALUES (1,'BCA');
/*!40000 ALTER TABLE `bank` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bank_account`
--

DROP TABLE IF EXISTS `bank_account`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bank_account` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `bank_id` int(11) DEFAULT NULL,
  `account_name` varchar(100) DEFAULT NULL,
  `account_no` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `bank_account_user_id_fk` (`user_id`),
  CONSTRAINT `bank_account_user_id_fk` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bank_account`
--

LOCK TABLES `bank_account` WRITE;
/*!40000 ALTER TABLE `bank_account` DISABLE KEYS */;
INSERT INTO `bank_account` VALUES (1,1,1,'KAWOKWOKA','12314512412312'),(2,2,1,'DD','333');
/*!40000 ALTER TABLE `bank_account` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `campaign`
--

DROP TABLE IF EXISTS `campaign`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `campaign` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `judul` varchar(100) DEFAULT NULL,
  `description` text,
  `date_start` datetime DEFAULT NULL,
  `date_end` datetime DEFAULT NULL,
  `is_sms` smallint(6) DEFAULT NULL,
  `is_apps` smallint(6) DEFAULT NULL,
  `is_email` smallint(6) DEFAULT NULL,
  `picture_2` varchar(255) DEFAULT NULL,
  `picture_3` varchar(255) DEFAULT NULL,
  `picture_4` varchar(255) DEFAULT NULL,
  `picture_1` varchar(255) DEFAULT NULL,
  `point_required` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `campaign`
--

LOCK TABLES `campaign` WRITE;
/*!40000 ALTER TABLE `campaign` DISABLE KEYS */;
INSERT INTO `campaign` VALUES (1,'test','asdsd','2018-04-08 01:00:00','2018-04-08 01:00:00',1,1,0,'uploads/call file group 2.jpg','uploads/IMG_13122017_133507_0.png','uploads/mbah surip.jpg','uploads/bagidata.png',1),(2,'Testing',' Lagi','2018-04-07 01:35:00','2018-04-07 01:35:00',1,0,1,'','','','uploads/IMG_28122017_085058_0.png',500),(3,'','',NULL,NULL,0,0,0,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `campaign` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `campaign_clustering`
--

DROP TABLE IF EXISTS `campaign_clustering`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `campaign_clustering` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `campaign_id` int(11) DEFAULT NULL,
  `clustering_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `campaign_clustering`
--

LOCK TABLES `campaign_clustering` WRITE;
/*!40000 ALTER TABLE `campaign_clustering` DISABLE KEYS */;
INSERT INTO `campaign_clustering` VALUES (2,3,1),(6,1,1),(7,1,2);
/*!40000 ALTER TABLE `campaign_clustering` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `campaign_overview_buy`
--

DROP TABLE IF EXISTS `campaign_overview_buy`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `campaign_overview_buy` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `campaign_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `campaign_overview_buy`
--

LOCK TABLES `campaign_overview_buy` WRITE;
/*!40000 ALTER TABLE `campaign_overview_buy` DISABLE KEYS */;
INSERT INTO `campaign_overview_buy` VALUES (1,1,1,'2018-04-17 18:49:46');
/*!40000 ALTER TABLE `campaign_overview_buy` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `campaign_overview_click`
--

DROP TABLE IF EXISTS `campaign_overview_click`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `campaign_overview_click` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `campaign_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `campaign_overview_click`
--

LOCK TABLES `campaign_overview_click` WRITE;
/*!40000 ALTER TABLE `campaign_overview_click` DISABLE KEYS */;
INSERT INTO `campaign_overview_click` VALUES (1,1,1,NULL),(2,1,1,'2018-04-17 18:49:35'),(3,1,1,'2018-04-17 18:50:52');
/*!40000 ALTER TABLE `campaign_overview_click` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `category`
--

LOCK TABLES `category` WRITE;
/*!40000 ALTER TABLE `category` DISABLE KEYS */;
INSERT INTO `category` VALUES (1,'Makanan'),(2,'Minuman');
/*!40000 ALTER TABLE `category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `clustering`
--

DROP TABLE IF EXISTS `clustering`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clustering` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `age_start` int(11) DEFAULT NULL,
  `age_end` int(11) DEFAULT NULL,
  `is_female` int(11) DEFAULT NULL,
  `is_male` int(11) DEFAULT NULL,
  `incoming_start` int(11) DEFAULT NULL,
  `incoming_end` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clustering`
--

LOCK TABLES `clustering` WRITE;
/*!40000 ALTER TABLE `clustering` DISABLE KEYS */;
INSERT INTO `clustering` VALUES (1,'Testing',10,30,1,1,123,1234),(2,'Testing2',10,23,1,1,123,1234);
/*!40000 ALTER TABLE `clustering` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `clustering_detail_category`
--

DROP TABLE IF EXISTS `clustering_detail_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clustering_detail_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `clustering_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clustering_detail_category`
--

LOCK TABLES `clustering_detail_category` WRITE;
/*!40000 ALTER TABLE `clustering_detail_category` DISABLE KEYS */;
INSERT INTO `clustering_detail_category` VALUES (4,1,2);
/*!40000 ALTER TABLE `clustering_detail_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `clustering_detail_interest`
--

DROP TABLE IF EXISTS `clustering_detail_interest`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clustering_detail_interest` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `clustering_id` int(11) DEFAULT NULL,
  `interest` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `clustering_detail_interest_clustering_id_fk` (`clustering_id`),
  CONSTRAINT `clustering_detail_interest_clustering_id_fk` FOREIGN KEY (`clustering_id`) REFERENCES `clustering` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clustering_detail_interest`
--

LOCK TABLES `clustering_detail_interest` WRITE;
/*!40000 ALTER TABLE `clustering_detail_interest` DISABLE KEYS */;
INSERT INTO `clustering_detail_interest` VALUES (12,2,'Bukukuan'),(21,1,'Bukukuan'),(22,1,'Cekcekcek');
/*!40000 ALTER TABLE `clustering_detail_interest` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `clustering_detail_location`
--

DROP TABLE IF EXISTS `clustering_detail_location`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clustering_detail_location` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `clustering_id` int(11) DEFAULT NULL,
  `location_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `clustering_detail_location_clustering_id_fk` (`clustering_id`),
  CONSTRAINT `clustering_detail_location_clustering_id_fk` FOREIGN KEY (`clustering_id`) REFERENCES `clustering` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clustering_detail_location`
--

LOCK TABLES `clustering_detail_location` WRITE;
/*!40000 ALTER TABLE `clustering_detail_location` DISABLE KEYS */;
INSERT INTO `clustering_detail_location` VALUES (25,2,1),(34,1,1),(35,1,2);
/*!40000 ALTER TABLE `clustering_detail_location` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `clustering_detail_marrital`
--

DROP TABLE IF EXISTS `clustering_detail_marrital`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clustering_detail_marrital` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `clustering_id` int(11) DEFAULT NULL,
  `marrital_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `clustering_detail_marrital_clustering_id_fk` (`clustering_id`),
  CONSTRAINT `clustering_detail_marrital_clustering_id_fk` FOREIGN KEY (`clustering_id`) REFERENCES `clustering` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clustering_detail_marrital`
--

LOCK TABLES `clustering_detail_marrital` WRITE;
/*!40000 ALTER TABLE `clustering_detail_marrital` DISABLE KEYS */;
INSERT INTO `clustering_detail_marrital` VALUES (26,2,1),(27,2,2),(28,2,3),(29,2,4),(38,1,3),(39,1,4);
/*!40000 ALTER TABLE `clustering_detail_marrital` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `clustering_detail_religion`
--

DROP TABLE IF EXISTS `clustering_detail_religion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clustering_detail_religion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `clustering_id` int(11) DEFAULT NULL,
  `religion_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `clustering_detail_religion_clustering_id_fk` (`clustering_id`),
  CONSTRAINT `clustering_detail_religion_clustering_id_fk` FOREIGN KEY (`clustering_id`) REFERENCES `clustering` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clustering_detail_religion`
--

LOCK TABLES `clustering_detail_religion` WRITE;
/*!40000 ALTER TABLE `clustering_detail_religion` DISABLE KEYS */;
INSERT INTO `clustering_detail_religion` VALUES (15,2,1),(16,2,2),(21,1,1);
/*!40000 ALTER TABLE `clustering_detail_religion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `job_title`
--

DROP TABLE IF EXISTS `job_title`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `job_title` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `job_title`
--

LOCK TABLES `job_title` WRITE;
/*!40000 ALTER TABLE `job_title` DISABLE KEYS */;
INSERT INTO `job_title` VALUES (1,'Job Title 1'),(2,'Job TItle 2');
/*!40000 ALTER TABLE `job_title` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `location`
--

DROP TABLE IF EXISTS `location`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `location` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(500) DEFAULT NULL,
  `country` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `location`
--

LOCK TABLES `location` WRITE;
/*!40000 ALTER TABLE `location` DISABLE KEYS */;
INSERT INTO `location` VALUES (1,'Jakarta','Indonesia'),(2,'Tangerang','Indonesia');
/*!40000 ALTER TABLE `location` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `marrital`
--

DROP TABLE IF EXISTS `marrital`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `marrital` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `marrital`
--

LOCK TABLES `marrital` WRITE;
/*!40000 ALTER TABLE `marrital` DISABLE KEYS */;
INSERT INTO `marrital` VALUES (1,'Lajang'),(2,'Menikah'),(3,'Cerai'),(4,'Cerai Ada Anak');
/*!40000 ALTER TABLE `marrital` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `menu`
--

DROP TABLE IF EXISTS `menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `parent` int(11) DEFAULT NULL,
  `route` varchar(255) DEFAULT NULL,
  `order` int(11) DEFAULT NULL,
  `data` blob,
  PRIMARY KEY (`id`),
  KEY `parent` (`parent`),
  CONSTRAINT `menu_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `menu` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menu`
--

LOCK TABLES `menu` WRITE;
/*!40000 ALTER TABLE `menu` DISABLE KEYS */;
/*!40000 ALTER TABLE `menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migration`
--

DROP TABLE IF EXISTS `migration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migration`
--

LOCK TABLES `migration` WRITE;
/*!40000 ALTER TABLE `migration` DISABLE KEYS */;
INSERT INTO `migration` VALUES ('m000000_000000_base',1522481634),('m130524_201442_init',1522481637),('m140209_132017_init',1522509056),('m140403_174025_create_account_table',1522509057),('m140504_113157_update_tables',1522509063),('m140504_130429_create_token_table',1522509066),('m140506_102106_rbac_init',1522782423),('m140602_111327_create_menu_table',1522782427),('m140830_171933_fix_ip_field',1522509067),('m140830_172703_change_account_table_name',1522509068),('m141222_110026_update_ip_field',1522509069),('m141222_135246_alter_username_length',1522509071),('m150614_103145_update_social_account_table',1522509074),('m150623_212711_fix_username_notnull',1522509074),('m151218_234654_add_timezone_to_profile',1522509076),('m160312_050000_create_user',1522782428),('m160929_103127_add_last_login_at_to_user_table',1522509078),('m170907_052038_rbac_add_index_on_auth_assignment_user_id',1522782423);
/*!40000 ALTER TABLE `migration` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `notification`
--

DROP TABLE IF EXISTS `notification`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `notification` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `notification` varchar(200) DEFAULT NULL,
  `read_at` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `notification_user_id_fk` (`user_id`),
  CONSTRAINT `notification_user_id_fk` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `notification`
--

LOCK TABLES `notification` WRITE;
/*!40000 ALTER TABLE `notification` DISABLE KEYS */;
/*!40000 ALTER TABLE `notification` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `page`
--

DROP TABLE IF EXISTS `page`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `page` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) DEFAULT NULL,
  `content` text,
  `active` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `page`
--

LOCK TABLES `page` WRITE;
/*!40000 ALTER TABLE `page` DISABLE KEYS */;
INSERT INTO `page` VALUES (1,'Term & Privacy Data','',1),(2,'Instagram Data Policy','',1),(3,'Twitter Data Policy','',1),(4,'Facebook Data Policy','',1),(5,'Google Plus Data Policy','',1),(6,'Youtube Data Policy','',1),(7,'Point & Reward Policy','',1),(8,'Receipt & Invoice Policy','',1);
/*!40000 ALTER TABLE `page` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `point_transaction`
--

DROP TABLE IF EXISTS `point_transaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `point_transaction` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `point_get` int(11) DEFAULT NULL,
  `previous_balance` int(11) DEFAULT NULL,
  `comment` varchar(100) DEFAULT NULL,
  `timestamp` datetime DEFAULT NULL,
  `debet_credit` varchar(1) DEFAULT NULL,
  `transaction_type` int(11) DEFAULT NULL COMMENT '0 = transaction/redeem\n1 = adjustment by admin\n2 = withdraw\n3 = addtion from receipt',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `point_transaction`
--

LOCK TABLES `point_transaction` WRITE;
/*!40000 ALTER TABLE `point_transaction` DISABLE KEYS */;
INSERT INTO `point_transaction` VALUES (3,1,120,0,'Hahahahha','2018-04-08 17:10:20','d',NULL),(4,1,-1,120,'Buying Campaign: 1','2018-04-12 14:35:37','c',NULL),(5,1,-1,119,'Buying Campaign: 1','2018-04-12 14:35:59','c',NULL),(6,2,12,0,'','2018-04-16 13:42:38','d',NULL),(7,1,-1,118,'okey','2018-04-17 09:51:04','c',2),(8,3,NULL,0,'','2018-04-17 18:31:45','c',3),(9,3,NULL,0,'','2018-04-17 18:33:12','c',3),(10,3,NULL,0,'','2018-04-17 18:33:45','c',3),(11,3,NULL,0,'','2018-04-17 18:34:20','c',3),(12,3,NULL,0,'','2018-04-17 18:34:43','c',3),(13,3,NULL,0,'','2018-04-17 18:35:08','c',3),(14,3,NULL,0,'','2018-04-17 18:35:09','c',3),(15,3,NULL,0,'','2018-04-17 18:35:10','c',3),(16,3,NULL,0,'','2018-04-17 18:35:34','c',3),(17,3,NULL,0,'','2018-04-17 18:36:05','c',3),(18,3,NULL,0,'','2018-04-17 18:36:12','c',3),(19,3,NULL,0,'','2018-04-17 18:36:25','c',3),(20,3,NULL,0,'','2018-04-17 18:36:45','c',3),(21,3,NULL,0,'','2018-04-17 18:39:36','c',3),(22,3,NULL,0,'','2018-04-17 18:39:54','c',3),(23,3,NULL,0,'','2018-04-17 18:40:04','c',3),(24,3,NULL,0,'','2018-04-17 18:40:16','c',3),(25,1,-1,117,'Buying Campaign: 1','2018-04-17 18:49:46','c',0);
/*!40000 ALTER TABLE `point_transaction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `profile`
--

DROP TABLE IF EXISTS `profile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `profile` (
  `user_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `public_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gravatar_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gravatar_id` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `location` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `website` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bio` text COLLATE utf8_unicode_ci,
  `blanja_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gender` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `twitter_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `twitter_time` datetime DEFAULT NULL,
  `instagram_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `instagram_time` datetime DEFAULT NULL,
  `bukalapak_time` datetime DEFAULT NULL,
  `facebook_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `facebook_time` datetime DEFAULT NULL,
  `gplus_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gplus_time` int(11) DEFAULT NULL,
  `religion` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `location_latitude` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `timezone` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `location_longitude` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `youtube_time` datetime DEFAULT NULL,
  `date_of_birth` date DEFAULT NULL,
  `pendapatan` double(16,2) DEFAULT NULL,
  `marrital_status` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bukalapak_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `youtube_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sendable_email` int(11) DEFAULT NULL,
  `sendable_sms_call` int(11) DEFAULT NULL,
  `job_title_id` int(11) DEFAULT NULL,
  `job_location_id` int(11) DEFAULT NULL,
  `job_location_address` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone_number` int(15) DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  CONSTRAINT `fk_user_profile` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `profile`
--

LOCK TABLES `profile` WRITE;
/*!40000 ALTER TABLE `profile` DISABLE KEYS */;
INSERT INTO `profile` VALUES (1,'DDASdsa',NULL,NULL,NULL,'1',NULL,NULL,NULL,'m',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1',NULL,NULL,NULL,NULL,'1994-04-07',300.00,'3',NULL,NULL,1,0,1,1,'asdasdas',466556),(2,NULL,NULL,NULL,NULL,'1',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1',NULL,NULL,NULL,NULL,'1994-04-07',300.00,'3',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(3,NULL,NULL,NULL,NULL,'1',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1',NULL,NULL,NULL,NULL,'1994-04-15',300.00,'3',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `profile` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `receipt`
--

DROP TABLE IF EXISTS `receipt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `receipt` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `receipt_1` varchar(255) DEFAULT NULL,
  `receipt_2` varchar(255) DEFAULT NULL,
  `receipt_3` varchar(255) DEFAULT NULL,
  `receipt_4` varchar(255) DEFAULT NULL,
  `receipt_5` varchar(255) DEFAULT NULL,
  `total_point` int(11) DEFAULT NULL,
  `total_price` double(16,2) DEFAULT NULL,
  `approved` int(11) DEFAULT NULL COMMENT '0 = not yet\n1 = approved\n-1 = not approved',
  `receipt_6` varchar(255) DEFAULT NULL,
  `receipt_7` varchar(255) DEFAULT NULL,
  `receipt_8` varchar(255) DEFAULT NULL,
  `receipt_9` varchar(255) DEFAULT NULL,
  `receipt_10` varchar(255) DEFAULT NULL,
  `receipt_type` enum('belanja','kereta','pesawat') DEFAULT NULL,
  `comment` text,
  `nama_toko` varchar(100) DEFAULT NULL,
  `alamat_toko` varchar(100) DEFAULT NULL,
  `tanggal_beli` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `receipt`
--

LOCK TABLES `receipt` WRITE;
/*!40000 ALTER TABLE `receipt` DISABLE KEYS */;
INSERT INTO `receipt` VALUES (2,1,'upload_recps/bagidata.png','upload_recps/call file group 1.jpg','upload_recps/call file group 2.jpg','upload_recps/hahahaha.jpg','upload_recps/setting asterisk.jpg',120,NULL,-1,'upload_recps/keju.jpg','upload_recps/gw banget dulu.jpg','upload_recps/gokong.jpg','upload_recps/bukti.png','upload_recps/IMG_13122017_133507_0.png',NULL,'Hahahahha',NULL,NULL,NULL),(3,2,'upload_recps/bukti.png','upload_recps/keju.jpg','upload_recps/gw banget dulu.jpg','upload_recps/call file group.jpg','upload_recps/call file group 2.jpg',12,NULL,-1,'upload_recps/hahahaha.jpg','upload_recps/setting asterisk.jpg','upload_recps/keju.jpg','upload_recps/call file group 1.jpg','upload_recps/bagidata.png','kereta','','asdasdas','adadas','2018-04-16 18:50:16'),(4,3,'upload_recps/bukti.png','upload_recps/keju.jpg','upload_recps/gw banget dulu.jpg','upload_recps/call file group.jpg','upload_recps/call file group 2.jpg',NULL,NULL,-1,'upload_recps/hahahaha.jpg','upload_recps/setting asterisk.jpg','upload_recps/keju.jpg','upload_recps/call file group 1.jpg','upload_recps/bagidata.png','kereta','','','',NULL);
/*!40000 ALTER TABLE `receipt` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `receipt_details`
--

DROP TABLE IF EXISTS `receipt_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `receipt_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `receipt_id` int(11) DEFAULT NULL,
  `items` varchar(100) DEFAULT NULL,
  `price` double(16,2) DEFAULT NULL,
  `total_item` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `receipt_details_receipt_id_fk` (`receipt_id`),
  CONSTRAINT `receipt_details_receipt_id_fk` FOREIGN KEY (`receipt_id`) REFERENCES `receipt` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `receipt_details`
--

LOCK TABLES `receipt_details` WRITE;
/*!40000 ALTER TABLE `receipt_details` DISABLE KEYS */;
INSERT INTO `receipt_details` VALUES (1,2,'Cekcekcek',120.00,25,1),(2,3,'Cekcekcek',120.00,252,1),(3,4,'Bukukuan',30.00,30,1),(7,4,'asdsadsa33333333333333333',3333.00,333,1);
/*!40000 ALTER TABLE `receipt_details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `religion`
--

DROP TABLE IF EXISTS `religion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `religion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `religion_name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `religion`
--

LOCK TABLES `religion` WRITE;
/*!40000 ALTER TABLE `religion` DISABLE KEYS */;
INSERT INTO `religion` VALUES (1,'Kristen'),(2,'Buddhis');
/*!40000 ALTER TABLE `religion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `social_account`
--

DROP TABLE IF EXISTS `social_account`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `social_account` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `provider` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `client_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `data` text COLLATE utf8_unicode_ci,
  `code` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `account_unique` (`provider`,`client_id`),
  UNIQUE KEY `account_unique_code` (`code`),
  KEY `fk_user_account` (`user_id`),
  CONSTRAINT `fk_user_account` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `social_account`
--

LOCK TABLES `social_account` WRITE;
/*!40000 ALTER TABLE `social_account` DISABLE KEYS */;
/*!40000 ALTER TABLE `social_account` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `token`
--

DROP TABLE IF EXISTS `token`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `token` (
  `user_id` int(11) NOT NULL,
  `code` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` int(11) NOT NULL,
  `type` smallint(6) NOT NULL,
  UNIQUE KEY `token_unique` (`user_id`,`code`,`type`),
  CONSTRAINT `fk_user_token` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `token`
--

LOCK TABLES `token` WRITE;
/*!40000 ALTER TABLE `token` DISABLE KEYS */;
INSERT INTO `token` VALUES (1,'X-gehAtza679Ze9HoMOkoY8Ac77AAXmO',1522512252,0);
/*!40000 ALTER TABLE `token` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `confirmed_at` int(11) DEFAULT NULL,
  `unconfirmed_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `blocked_at` int(11) DEFAULT NULL,
  `registration_ip` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `flags` int(11) NOT NULL DEFAULT '0',
  `last_login_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_unique_username` (`username`),
  UNIQUE KEY `user_unique_email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'admin','richardoctoey@gmail.com','$2y$10$YXA8jeYihOcJGWSKt8Dwn.QJNNjZ0JCfB5CNxSsmznynwF2evtHsG','zt8wng90QT4LkecEaR5qc-BwidSz3P-7',2018,NULL,NULL,'::1',1522512252,1522512252,1,1523956540),(2,'testing','testing@gmail.com','$2y$10$YXA8jeYihOcJGWSKt8Dwn.QJNNjZ0JCfB5CNxSsmznynwF2evtHsG','zt8wng90QT4LkecEaR5qc-BwidSz3P-7',2018,NULL,NULL,'::1',1522512252,1522512252,1,1523668203),(3,'testing2','testing@gmail.coms','$2y$10$YXA8jeYihOcJGWSKt8Dwn.QJNNjZ0JCfB5CNxSsmznynwF2evtHsG','zt8wng90QT4LkecEaR5qc-BwidSz3P-7',2018,NULL,NULL,'::1',1522512252,1522512252,1,1523668203);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `view_debet_credit`
--

DROP TABLE IF EXISTS `view_debet_credit`;
/*!50001 DROP VIEW IF EXISTS `view_debet_credit`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `view_debet_credit` (
  `id` tinyint NOT NULL,
  `user_id` tinyint NOT NULL,
  `point_get` tinyint NOT NULL,
  `previous_point` tinyint NOT NULL,
  `comment` tinyint NOT NULL,
  `transaction_type` tinyint NOT NULL,
  `debet_credit` tinyint NOT NULL,
  `timestamp` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `wallet_transaction`
--

DROP TABLE IF EXISTS `wallet_transaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wallet_transaction` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `point_get` int(11) DEFAULT NULL,
  `previous_point` int(11) DEFAULT NULL,
  `comment` varchar(50) DEFAULT NULL,
  `transaction_type` int(11) DEFAULT NULL COMMENT '0 = transaction/redeem\n1 = adjustment by admin\n2 = withdraw',
  `debet_credit` varchar(1) DEFAULT NULL,
  `timestamp` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wallet_transaction`
--

LOCK TABLES `wallet_transaction` WRITE;
/*!40000 ALTER TABLE `wallet_transaction` DISABLE KEYS */;
INSERT INTO `wallet_transaction` VALUES (1,1,250,0,NULL,3,'d','2018-04-07 17:22:07'),(2,1,100,250,NULL,3,'d','2018-04-07 17:22:10'),(3,1,-300,350,NULL,0,'c','2018-04-06 17:22:12'),(7,1,-1,50,'Buying Campaign: 1',NULL,'c',NULL),(8,1,-1,49,'Buying Campaign: 1',NULL,'c',NULL);
/*!40000 ALTER TABLE `wallet_transaction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `withdraw_point`
--

DROP TABLE IF EXISTS `withdraw_point`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `withdraw_point` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `bank_account_id` int(11) DEFAULT NULL,
  `balance_on_request` int(11) DEFAULT NULL,
  `approved` int(11) DEFAULT NULL COMMENT '-1 = Rejected\n0 = Unprocessed\n1 = Processed',
  `request_time` datetime DEFAULT NULL,
  `approve_time` datetime DEFAULT NULL,
  `comment` varchar(200) DEFAULT NULL,
  `request` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `withdraw_point_user_id_fk` (`user_id`),
  CONSTRAINT `withdraw_point_user_id_fk` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `withdraw_point`
--

LOCK TABLES `withdraw_point` WRITE;
/*!40000 ALTER TABLE `withdraw_point` DISABLE KEYS */;
INSERT INTO `withdraw_point` VALUES (1,1,1,NULL,1,'2018-04-17 06:56:49','2018-04-17 09:39:27','asdasdas',12312),(2,1,1,118,-1,'2018-04-17 09:40:00','2018-04-17 09:44:38','Balance TIdak mencukupi',4124123),(3,1,1,118,1,'2018-04-17 09:50:48','2018-04-17 09:51:03','okey',1),(4,1,1,117,0,'2018-04-17 11:18:34',NULL,NULL,23333);
/*!40000 ALTER TABLE `withdraw_point` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Final view structure for view `view_debet_credit`
--

/*!50001 DROP TABLE IF EXISTS `view_debet_credit`*/;
/*!50001 DROP VIEW IF EXISTS `view_debet_credit`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `view_debet_credit` AS select `debet`.`id` AS `id`,`debet`.`user_id` AS `user_id`,`debet`.`point_get` AS `point_get`,`debet`.`previous_point` AS `previous_point`,`debet`.`comment` AS `comment`,`debet`.`transaction_type` AS `transaction_type`,`debet`.`debet_credit` AS `debet_credit`,`debet`.`timestamp` AS `timestamp` from `wallet_transaction` `debet` where (`debet`.`debet_credit` = 'd') union select `credit`.`id` AS `id`,`credit`.`user_id` AS `user_id`,`credit`.`point_get` AS `point_get`,`credit`.`previous_point` AS `previous_point`,`credit`.`comment` AS `comment`,`credit`.`transaction_type` AS `transaction_type`,`credit`.`debet_credit` AS `debet_credit`,`credit`.`timestamp` AS `timestamp` from `wallet_transaction` `credit` where (`credit`.`debet_credit` = 'c') */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-04-18  1:36:27
