<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
/* @var $this yii\web\View */
/* @var $model backend\models\Campaign */
/* @var $form yii\widgets\ActiveForm */
use kartik\datetime\DateTimePicker;
use nex\chosen\Chosen;
?>

<div class="campaign-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'judul')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'date_start')->widget(DateTimePicker::classname()) ?>

    <?= $form->field($model, 'date_end')->widget(DateTimePicker::classname()) ?>

    <?= $form->field($model, 'is_sms')->checkbox() ?>

    <?= $form->field($model, 'is_apps')->checkbox() ?>

    <?= $form->field($model, 'is_email')->checkbox() ?>

    <?= $form->field($model, 'picture_1')->fileInput(['accept' => 'image/*']) ?>

    <?= $form->field($model, 'picture_2')->fileInput(['accept' => 'image/*']) ?>

    <?= $form->field($model, 'picture_3')->fileInput(['accept' => 'image/*']) ?>

    <?= $form->field($model, 'picture_4')->fileInput(['accept' => 'image/*']) ?>

    <?=
    $form->field($model, 'clusterings')->widget(
        Chosen::className(), [
        'items' => \yii\helpers\ArrayHelper::map(\backend\models\Clustering::find()->all(),'id','name'),
        'multiple' => true,
        'clientOptions' => [
            'search_contains' => true,
            'single_backstroke_delete' => false,
        ]
    ]);
    ?>

    <?= $form->field($model, 'point_required')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
