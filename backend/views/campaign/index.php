<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\CampaignSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Campaigns';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="campaign-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Campaign', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php if(Yii::$app->session->hasFlash('success-exchange')): ?>
        <div class="alert alert-success">
            <?= Yii::$app->session->getFlash('success-exchange') ?>
        </div>
    <?php endif; ?>

    <?php if(Yii::$app->session->hasFlash('failed-exchange')): ?>
        <div class="alert alert-danger">
            <?= Yii::$app->session->getFlash('failed-exchange') ?>
        </div>
    <?php endif; ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'judul',
            'date_start',
            'date_end',
            'ovViewCount',
            'ovBuyCount',
            //'is_sms',
            //'is_apps',
            //'is_email:email',
            //'picture_2',
            //'picture_3',
            //'picture_4',
            //'picture_1',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view} {update} {delete} {buy}',
                'buttons' => [
                        'buy' => function($url, $model){
                            return Html::a("<span class='glyphicon glyphicon-tag'></span>", $url, [
                                    'title' => 'Buy'
                            ]);
                        }
                ]
            ],
        ],
    ]); ?>
</div>
