<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use nex\chosen\Chosen;

/* @var $this yii\web\View */
/* @var $model backend\models\BankAccount */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="bank-account-form">

    <?php $form = ActiveForm::begin(); ?>

    <?=
    $form->field($model, 'bank_id')->widget(
        Chosen::className(), [
        'items' => \yii\helpers\ArrayHelper::map(\backend\models\Bank::find()->all(),'id','name'),
        'multiple' => false,
        'clientOptions' => [
            'search_contains' => true,
            'single_backstroke_delete' => false,
        ]
    ]);
    ?>

    <?= $form->field($model, 'account_no')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'account_name')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
