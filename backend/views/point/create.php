<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\PointTransaction */

$this->title = 'Create Point Transaction';
$this->params['breadcrumbs'][] = ['label' => 'Point Transactions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="point-transaction-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
