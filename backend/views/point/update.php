<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\PointTransaction */

$this->title = 'Update Point Transaction: {nameAttribute}';
$this->params['breadcrumbs'][] = ['label' => 'Point Transactions', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="point-transaction-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
