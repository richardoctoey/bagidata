<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\WalletTransactionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Wallet Transactions';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="wallet-transaction-index">

    <h1>Debit Credit Report</h1>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'point_get',
            'previous_balance',
            'comment',
            //'transaction_type',
            'debet_credit',
            //'timestamp',
        ],
    ]); ?>

</div>

<div class="row">
    <div class="col-xs-3 col-md-3 col-lg-3">
        <table class="table table-striped table-bordered">
            <tbody>
            <tr>
                <td>Total Debet</td>
                <td>:</td>
                <td><?= $totalDebit ?></td>
            </tr>
            </tbody>

            <tbody>
            <tr>
                <td>Total Credit</td>
                <td>:</td>
                <td><?= $totalCredit ?></td>
            </tr>
            </tbody>
        </table>
    </div>
</div>
