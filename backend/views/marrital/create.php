<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Marrital */

$this->title = 'Create Marrital';
$this->params['breadcrumbs'][] = ['label' => 'Marritals', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="marrital-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
