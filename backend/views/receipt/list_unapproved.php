<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\ReceiptSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Receipts';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="receipt-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Receipt', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'user_id',
            'receipt_1',
            'receipt_2',
            'receipt_3',
            //'receipt_4',
            //'receipt_5',
            //'total_point',
            //'total_price',
            //'approved',
            //'receipt_6',
            //'receipt_7',
            //'receipt_8',
            //'receipt_9',
            //'receipt_10',

            [
                'class' => 'yii\grid\ActionColumn',
                'buttons' => [
                    'process' => function($url, $model){
                        return Html::a("<span class='glyphicon glyphicon-refresh'></span>", $url, [
                            'title' => 'Proses'
                        ]);
                    }
                ],
                'template' => '{process}'
            ],
        ],
    ]); ?>
</div>
