<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Receipt */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="receipt-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'receipt_type')->dropDownList([
        'belanja'=>'Belanjaan',
        'kereta'=>'Tiket Kereta',
        'pesawat'=>'Tiket Pesawat'
    ]) ?>

    <?= $form->field($model, 'receipt_1')->fileInput(['accept' => 'image/*']) ?>

    <?= $form->field($model, 'receipt_2')->fileInput(['accept' => 'image/*']) ?>

    <?= $form->field($model, 'receipt_3')->fileInput(['accept' => 'image/*']) ?>

    <?= $form->field($model, 'receipt_4')->fileInput(['accept' => 'image/*']) ?>

    <?= $form->field($model, 'receipt_5')->fileInput(['accept' => 'image/*']) ?>

    <?= $form->field($model, 'receipt_6')->fileInput(['accept' => 'image/*']) ?>

    <?= $form->field($model, 'receipt_7')->fileInput(['accept' => 'image/*']) ?>

    <?= $form->field($model, 'receipt_8')->fileInput(['accept' => 'image/*']) ?>

    <?= $form->field($model, 'receipt_9')->fileInput(['accept' => 'image/*']) ?>

    <?= $form->field($model, 'receipt_10')->fileInput(['accept' => 'image/*']) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
