<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\ReceiptSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="receipt-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'user_id') ?>

    <?= $form->field($model, 'receipt_1') ?>

    <?= $form->field($model, 'receipt_2') ?>

    <?= $form->field($model, 'receipt_3') ?>

    <?php // echo $form->field($model, 'receipt_4') ?>

    <?php // echo $form->field($model, 'receipt_5') ?>

    <?php // echo $form->field($model, 'total_point') ?>

    <?php // echo $form->field($model, 'total_price') ?>

    <?php // echo $form->field($model, 'approved') ?>

    <?php // echo $form->field($model, 'receipt_6') ?>

    <?php // echo $form->field($model, 'receipt_7') ?>

    <?php // echo $form->field($model, 'receipt_8') ?>

    <?php // echo $form->field($model, 'receipt_9') ?>

    <?php // echo $form->field($model, 'receipt_10') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
