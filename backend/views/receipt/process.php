<?php

use yii\helpers\Html;
use wbraganca\dynamicform\DynamicFormWidget;
use yii\widgets\ActiveForm;
use nex\chosen\Chosen;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\ReceiptSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Receipts';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="receipt-form">
    <?php $form = ActiveForm::begin(['id'=>'dynamic-form']); ?>

    <div class="receipt-form">
        <?= $form->field($model, 'approved')->dropDownList([-1=>'Reject', 1=>'Approve']) ?>
        <?= $form->field($model, 'comment')->textarea() ?>
        <?= $form->field($model, 'total_point')->textInput() ?>
        <?= $form->field($model, "nama_toko")->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, "alamat_toko")->textInput(['maxlength' => true]) ?>
        <?=
        $form->field($model, 'tanggal_beli')->widget(
            \kartik\widgets\DateTimePicker::className(), [
            'pluginOptions' => [
                'format' => 'yyyy-mm-dd hh:ii:ss'
            ]
        ]);
        ?>

    </div>

    <div class="panel panel-default">
        <div class="panel-heading"><h4><i class="glyphicon glyphicon-envelope"></i> Details</h4></div>
        <div class="panel-body">
            <?php \common\widgets\DynamicFormWidgetBagiData::begin([
                'widgetContainer' => 'dynamicform_wrapper', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
                'widgetBody' => '.container-items', // required: css class selector
                'widgetItem' => '.item', // required: css class
                'min' => 1, // 0 or 1 (default 1)
                'insertButton' => '.add-item', // css class
                'deleteButton' => '.remove-item', // css class
                'model' => $modelDetails[0],
                'formId' => 'dynamic-form',
                'formFields' => [
                    'items',
                    'price',
                    'address_line2',
                    'total_item',
                    'category_id',
                ],
            ]); ?>

            <div class="container-items"><!-- widgetContainer -->
                <?php foreach ($modelDetails as $i => $modelDetail): ?>
                    <div class="item panel panel-default"><!-- widgetBody -->
                        <div class="panel-heading">
                            <h3 class="panel-title pull-left">Item</h3>
                            <div class="pull-right">
                                <button type="button" class="add-item btn btn-success btn-xs"><i class="glyphicon glyphicon-plus"></i></button>
                                <button type="button" class="remove-item btn btn-danger btn-xs"><i class="glyphicon glyphicon-minus"></i></button>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="panel-body">
                            <?php
                            // necessary for update action.
                            if (!$modelDetail->isNewRecord) {
                                echo Html::activeHiddenInput($modelDetail, "[{$i}]id");
                            }
                            ?>
                            <?= $form->field($modelDetail, "[{$i}]items")->textInput(['maxlength' => true]) ?>
                            <div class="row">
                                <div class="col-sm-4">
                                    <?= $form->field($modelDetail, "[{$i}]price")->textInput(['maxlength' => true]) ?>
                                </div>
                                <div class="col-sm-4">
                                    <?= $form->field($modelDetail, "[{$i}]total_item")->textInput(['maxlength' => true]) ?>
                                </div>
                                <div class="col-sm-4">
                                    <?= $form->field($modelDetail, "[{$i}]category_id")->widget(
                                        \common\widgets\Select2BagiData::className(), [
                                        'data' => \yii\helpers\ArrayHelper::map(\backend\models\Category::find()->all(),'id','name'),
                                        'options' => [
                                                'multiple' => false,
                                        ]
                                    ]); ?>
                                </div>
                            </div><!-- .row -->
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
            <?php \common\widgets\DynamicFormWidgetBagiData::end(); ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>