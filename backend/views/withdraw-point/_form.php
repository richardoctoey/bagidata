<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use nex\chosen\Chosen;

/* @var $this yii\web\View */
/* @var $model backend\models\WithdrawPoint */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="withdraw-point-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'request')->textInput() ?>

    <?= $form->field($model, 'bank_account_id')->textInput() ?>

    <?=
    $form->field($model, 'bank_account_id')->dropDownList(\yii\helpers\ArrayHelper::map(\backend\models\BankAccount::bankAccounts(Yii::$app->user->id),'id','bank.name'));
    ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
