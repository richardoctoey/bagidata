<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\WithdrawPointSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="withdraw-point-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'user_id') ?>

    <?= $form->field($model, 'request') ?>

    <?= $form->field($model, 'balance_on_request') ?>

    <?= $form->field($model, 'approved') ?>

    <?php // echo $form->field($model, 'request_time') ?>

    <?php // echo $form->field($model, 'approve_time') ?>

    <?php // echo $form->field($model, 'comment') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
