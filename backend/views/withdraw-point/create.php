<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\WithdrawPoint */

$this->title = 'Create Withdraw Point';
$this->params['breadcrumbs'][] = ['label' => 'Withdraw Points', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="withdraw-point-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
