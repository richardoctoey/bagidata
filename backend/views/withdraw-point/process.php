<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\WithdrawPoint */

?>
<div class="withdraw-point-update">

    <h1>Process Withdraw</h1>

    <div class="withdraw-point-form">

        <?php $form = ActiveForm::begin(); ?>

        <p>Request Withdraw Point: <?= $model->request ?></p>
        <p>Request Transfer To: <?= $model->bankInfo->bank->name ?></p>
        <p>Request Transfer To: <?= $model->bankInfo->account_no ?></p>

        <?= $form->errorSummary($model) ?>

        <?= $form->field($model, 'approved')->dropDownList(['1'=>'Approved', '-1' => 'Reject']) ?>
        <?= $form->field($model, 'comment')->textInput() ?>

        <div class="form-group">
            <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>

</div>
