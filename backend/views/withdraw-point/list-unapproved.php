<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\WithdrawPointSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Withdraw Points';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="withdraw-point-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Withdraw Point', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'user_id',
            'request',
            'balance_on_request',
            'approved',
            //'request_time',
            //'approve_time',
            //'comment',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{process}',
                'buttons' => [
                    'process' => function($url, $model){
                        return Html::a("<span class='glyphicon glyphicon-refresh'></span>", $url, [
                            'title' => 'Proses'
                        ]);
                    }
                ],
            ],
        ],
    ]); ?>
</div>
