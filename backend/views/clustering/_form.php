<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use nex\chosen\Chosen;

/* @var $this yii\web\View */
/* @var $model backend\models\Clustering */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="clustering-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'age_start')->textInput() ?>

    <?= $form->field($model, 'age_end')->textInput() ?>

    <?= $form->field($model, 'is_female')->checkbox() ?>

    <?= $form->field($model, 'is_male')->checkbox() ?>

    <?= $form->field($model, 'incoming_start')->textInput() ?>

    <?= $form->field($model, 'incoming_end')->textInput() ?>

    <?= $form->field($model, 'religions')->checkboxList(\yii\helpers\ArrayHelper::map(\backend\models\Religion::find()->all(),'id','religion_name')) ?>

    <?= $form->field($model, 'marritals')->checkboxList(\yii\helpers\ArrayHelper::map(\backend\models\Marrital::find()->all(),'id','name')) ?>

    <?=
    $form->field($model, 'locations')->widget(
        Chosen::className(), [
        'items' => \yii\helpers\ArrayHelper::map(\backend\models\Location::find()->all(),'id','name'),
        'multiple' => true,
        'clientOptions' => [
            'search_contains' => true,
            'single_backstroke_delete' => false,
        ]
    ]);
    ?>

    <h3>Behavior</h3>

    <?=
    $form->field($model, 'interests')->widget(
        Chosen::className(), [
        'items' => \yii\helpers\ArrayHelper::map(\backend\models\ClusteringDetailInterest::getAllInterestGrouppedBy(),'id','name'),
        'multiple' => true,
        'clientOptions' => [
            'search_contains' => true,
            'single_backstroke_delete' => false,
        ]
    ]);
    ?>

    <?=
    $form->field($model, 'categories')->widget(
        Chosen::className(), [
        'items' => \yii\helpers\ArrayHelper::map(\backend\models\Category::find()->all(),'id','name'),
        'multiple' => true,
        'clientOptions' => [
            'search_contains' => true,
            'single_backstroke_delete' => false,
        ]
    ]);
    ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
