<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Clustering */

$this->title = 'Update Clustering: {nameAttribute}';
$this->params['breadcrumbs'][] = ['label' => 'Clusterings', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="clustering-update">

    <h1><?= Html::encode($model->name) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
