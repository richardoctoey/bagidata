<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Clustering */

$this->title = 'Create Clustering';
$this->params['breadcrumbs'][] = ['label' => 'Clusterings', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="clustering-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
