<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\ClusteringSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Clusterings';
$this->params['breadcrumbs'][] = $this->title;

$this->registerJs("var charts = []", \yii\web\View::POS_HEAD);
?>
<div class="clustering-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Clustering', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'id',
            'name',
            'age_start',
            'age_end',
            'totalMale',
            'totalFemale',
            'incoming_start',
            'incoming_end',
            'age_start',
            'age_end',

            [
                'class' => 'yii\grid\ActionColumn',
                'buttons' => [
                    'addjs' => function($url, $model){
                        return "<script>charts.push()</script>";
                    }
                ],
                'template' => '{view} {update} {delete} {addjs}'
            ],
        ],
    ]); ?>
</div>
