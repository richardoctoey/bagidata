<?php

/*
 * This file is part of the Dektrium project
 *
 * (c) Dektrium project <http://github.com/dektrium>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var dektrium\user\models\User $user
 * @var dektrium\user\models\Profile $profile
 */
?>

<?php $this->beginContent('@backend/views/user/admin/update.php', ['user' => $user]) ?>

<?php $form = ActiveForm::begin([
    'layout' => 'horizontal',
    'enableAjaxValidation' => true,
    'enableClientValidation' => false,
    'fieldConfig' => [
        'horizontalCssClasses' => [
            'wrapper' => 'col-sm-9',
        ],
    ],
]); ?>

<p>Percentage: <?=$percentage?>/100</p>


<?= $form->field($profile, 'name') ?>
<?=
$form->field($profile, 'date_of_birth')->widget(
    \kartik\widgets\DatePicker::className(), [
    'pluginOptions' => [
        'format' => 'yyyy-mm-dd'
    ]
]);
?>

<?=
$form->field($profile, 'phone_number')->textInput();
?>

<?=
$form->field($profile, 'religion')->widget(
    \nex\chosen\Chosen::className(), [
    'items' => \yii\helpers\ArrayHelper::map(\backend\models\Religion::find()->all(),'id','religion_name'),
    'multiple' => false,
    'clientOptions' => [
        'search_contains' => true,
        'single_backstroke_delete' => false,
    ]
]);
?>

<?=
$form->field($profile, 'location')->widget(
    \nex\chosen\Chosen::className(), [
    'items' => \yii\helpers\ArrayHelper::map(\backend\models\Location::find()->all(),'id','name'),
    'multiple' => false,
    'clientOptions' => [
        'search_contains' => true,
        'single_backstroke_delete' => false,
    ]
]);
?>

<?=
$form->field($profile, 'job_location_id')->widget(
    \nex\chosen\Chosen::className(), [
    'items' => \yii\helpers\ArrayHelper::map(\backend\models\Location::find()->all(),'id','name'),
    'multiple' => false,
    'clientOptions' => [
        'search_contains' => true,
        'single_backstroke_delete' => false,
    ]
]);
?>

<?=
$form->field($profile, 'pendapatan')->textInput();
?>

<?=
$form->field($profile, 'job_title_id')->widget(
    \nex\chosen\Chosen::className(), [
    'items' => \yii\helpers\ArrayHelper::map(\backend\models\JobTitle::find()->all(),'id','name'),
    'multiple' => false,
    'clientOptions' => [
        'search_contains' => true,
        'single_backstroke_delete' => false,
    ]
]);
?>

<?=
$form->field($profile, 'gender')->dropDownList(['m'=>'Male','f'=>'Female']);
?>

<?=
$form->field($profile, 'job_location_address')->textarea();
?>

<?=
$form->field($profile, 'sendable_email')->checkbox();
?>

<?=
$form->field($profile, 'sendable_sms_call')->checkbox();
?>

<div class="form-group">
    <div class="col-lg-offset-3 col-lg-9">
        <?= Html::submitButton(Yii::t('user', 'Update'), ['class' => 'btn btn-block btn-success']) ?>
    </div>
</div>

<?php ActiveForm::end(); ?>

<?php $this->endContent() ?>
