<?php
/**
 * Created by PhpStorm.
 * User: Acer
 * Date: 16/04/2018
 * Time: 18.03
 */

namespace backend\models;


use dektrium\user\models\Profile;

class ProfileBagiData extends Profile
{
    public function rules()
    {
        return [
            [['date_of_birth','job_location_address','name','gender'], 'string'],
            [['phone_number','religion','location','sendable_email','sendable_sms_call','pendapatan','job_location_id','job_title_id'], 'integer',],
        ];
    }

    public function getFilledData(){
        $total_all = 0;
        $total = 0;
        if(strlen($this->name)>0){
            $total+=1;
            $total_all+=1;
        }else{
            $total_all+=1;
        }
        if(strlen($this->date_of_birth)>0){
            $total+=1;
            $total_all+=1;
        }else{
            $total_all+=1;
        }
        if(strlen($this->phone_number)>0){
            $total+=1;
            $total_all+=1;
        }else{
            $total_all+=1;
        }
        if(strlen($this->religion)>0){
            $total+=1;
            $total_all+=1;
        }else{
            $total_all+=1;
        }
        if(strlen($this->location)>0){
            $total+=1;
            $total_all+=1;
        }else{
            $total_all+=1;
        }
        if(strlen($this->job_location_id)>0){
            $total+=1;
            $total_all+=1;
        }else{
            $total_all+=1;
        }
        if(strlen($this->pendapatan)>0){
            $total+=1;
            $total_all+=1;
        }else{
            $total_all+=1;
        }
        if(strlen($this->job_title_id)>0){
            $total+=1;
            $total_all+=1;
        }else{
            $total_all+=1;
        }
        if(strlen($this->job_location_address)>0){
            $total+=1;
            $total_all+=1;
        }else{
            $total_all+=1;
        }

        return ceil($total/$total_all*100);
    }


}