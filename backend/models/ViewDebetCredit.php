<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "view_debet_credit".
 *
 * @property string $id
 * @property int $user_id
 * @property int $point_get
 * @property int $previous_point
 * @property string $comment
 * @property int $transaction_type
 * @property string $debet_credit
 * @property string $timestamp
 */
class ViewDebetCredit extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'view_debet_credit';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'point_get', 'previous_point', 'transaction_type'], 'integer'],
            [['timestamp'], 'safe'],
            [['comment'], 'string', 'max' => 50],
            [['debet_credit'], 'string', 'max' => 1],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'point_get' => 'Point Get',
            'previous_point' => 'Previous Point',
            'comment' => 'Comment',
            'transaction_type' => 'Transaction Type',
            'debet_credit' => 'Debet Credit',
            'timestamp' => 'Timestamp',
        ];
    }
}
