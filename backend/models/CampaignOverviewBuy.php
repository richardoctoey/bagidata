<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "campaign_overview_buy".
 *
 * @property int $id
 * @property int $campaign_id
 * @property int $user_id
 * @property string $datetime
 */
class CampaignOverviewBuy extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'campaign_overview_buy';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['campaign_id', 'user_id'], 'integer'],
            [['datetime'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'campaign_id' => 'Campaign ID',
            'user_id' => 'User ID',
            'datetime' => 'Datetime',
        ];
    }
}
