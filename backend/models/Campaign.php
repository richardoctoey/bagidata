<?php

namespace backend\models;

use Yii;
use yii\web\UploadedFile;

/**
 * This is the model class for table "campaign".
 *
 * @property int $id
 * @property string $judul
 * @property string $description
 * @property string $date_start
 * @property string $date_end
 * @property int $is_sms
 * @property int $is_apps
 * @property int $is_email
 * @property string $picture_2
 * @property string $picture_3
 * @property string $picture_4
 * @property string $picture_1
 * @property string $point_required
 *
 * @property AnalyticsClustering[] $analyticsClusterings
 */
class Campaign extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'campaign';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['description'], 'string'],
            [['date_start', 'date_end'], 'safe'],
            [['is_sms', 'is_apps', 'is_email', 'point_required'], 'integer'],
            [['judul'], 'string', 'max' => 100],
            //[['picture_2', 'picture_3', 'picture_4', 'picture_1'], 'string', 'max' => 255],
            [['picture_1','picture_2','picture_3','picture_4'], 'file', 'skipOnEmpty' => true]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [    
            'judul' => 'Judul',
            'description' => 'Description',
            'date_start' => 'Date Start',
            'date_end' => 'Date End',
            'is_sms' => 'Is Sms',
            'is_apps' => 'Is Apps',
            'is_email' => 'Is Email',
            'picture_2' => 'Picture 2',
            'picture_3' => 'Picture 3',
            'picture_4' => 'Picture 4',
            'picture_1' => 'Picture 1',
            'point_required' => 'Cash Required'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAnalyticsClusterings()
    {
        return $this->hasMany(AnalyticsClustering::className(), ['campaign_id' => 'id']);
    }

    public function upload(){
        $picture_1 = UploadedFile::getInstance($this, 'picture_1');
        $picture_2 = UploadedFile::getInstance($this, 'picture_2');
        $picture_3 = UploadedFile::getInstance($this, 'picture_3');
        $picture_4 = UploadedFile::getInstance($this, 'picture_4');

        if ($this->validate()) {

            if($picture_1 != null){
                $url_1 = 'uploads/' . $picture_1->baseName . '.' . $picture_1->extension;
                $picture_1->saveAs($url_1);
                $this->picture_1 = $url_1;
            }else{
                $this->picture_1 = $this->getOldAttribute('picture_1');
            }

            if($picture_2 != null){
                $url_2 = 'uploads/' . $picture_2->baseName . '.' . $picture_2->extension;
                $picture_2->saveAs($url_2);
                $this->picture_2 = $url_2;
            }else{
                $this->picture_2 = $this->getOldAttribute('picture_2');
            }

            if($picture_3 != null){
                $url_3 = 'uploads/' . $picture_3->baseName . '.' . $picture_3->extension;
                $picture_3->saveAs($url_3);
                $this->picture_3 = $url_3;
            }else{
                $this->picture_3 = $this->getOldAttribute('picture_3');
            }

            if($picture_4 != null){
                $url_4 = 'uploads/' . $picture_4->baseName . '.' . $picture_4->extension;
                $picture_4->saveAs($url_4);
                $this->picture_4 = $url_4;
            }else{
                $this->picture_4 = $this->getOldAttribute('picture_4');
            }

            return true;

        } else {
            return false;
        }
    }

    public function getClusterings(){
        return $this->hasMany(Clustering::className(), ['id' => 'clustering_id'])
            ->viaTable(CampaignClustering::tableName(), ['campaign_id' => 'id']);
    }

    public function getClusteringIds(){
        $ids = [];
        foreach($this->clusterings as $cluster){
            $ids[] = $cluster->id;
        }
        return $ids;
    }

    public function process($data){
        CampaignClustering::deleteAll(['campaign_id' => $this->id]);
        if(isset($data['Campaign']['clusterings'])) {
            foreach ($data['Campaign']['clusterings'] as $m) {
                $objInterest = new CampaignClustering();
                $objInterest->campaign_id = $this->id;
                $objInterest->clustering_id = $m;
                $objInterest->save(false);
            }
        }
    }

    public static function getTotalReachPeople($clusterIds, $baseQueryOnly=false){
        $connection = Yii::$app->getDb();
        $lastArray = end($clusterIds);
        $querys = [];
        $counter = 0;

        $qs = "";
        foreach($clusterIds as $clusterId){
            $querys[] .= " SELECT id FROM user u
            INNER JOIN profile p ON u.id = p.user_id
            WHERE p.marrital_status IN (SELECT marrital_id FROM clustering_detail_marrital WHERE clustering_id = :clustering_id)
            AND TIMESTAMPDIFF(YEAR,p.date_of_birth,CURDATE()) BETWEEN (SELECT age_start FROM clustering WHERE id = 1) AND (SELECT age_end FROM clustering WHERE id = :clustering_id)
            AND p.pendapatan BETWEEN (SELECT incoming_start FROM clustering WHERE id = 1) AND (SELECT incoming_end FROM clustering WHERE id = :clustering_id)
            AND p.religion IN (SELECT religion_id FROM clustering_detail_religion WHERE clustering_id = :clustering_id)
            AND u.id IN (SELECT r.user_id FROM receipt r INNER JOIN receipt_details rd ON r.id = rd.receipt_id WHERE rd.items IN (SELECT cdi.interest FROM clustering_detail_interest cdi WHERE clustering_id = :clustering_id))
            AND u.id IN (SELECT r.user_id FROM receipt r INNER JOIN receipt_details rd ON r.id = rd.receipt_id WHERE rd.category_id IN (SELECT cdc.category_id FROM clustering_detail_category cdc WHERE clustering_id = :clustering_id))
            AND p.location IN (SELECT location_id FROM clustering_detail_location WHERE clustering_id = :clustering_id)
            AND ((IF((SELECT is_male FROM clustering WHERE id=1), p.gender, 0) LIKE 'm') OR ((IF((SELECT is_female FROM clustering WHERE id=1), p.gender, 0) LIKE 'f')))
            ";
            $querys[$counter] = str_replace(":clustering_id",$clusterId,$querys[$counter]);
            $qs .= $querys[$counter];

            if($lastArray != $clusterId){
                $qs .= " UNION ";
            }

            $counter+=1;
        }

        if(count($querys)==0){
            return 0;
        }

        $queryAll = "SELECT * FROM ($qs) q GROUP BY id";
        if($baseQueryOnly){
            return $qs;
        }

        $command = $connection->createCommand($queryAll);
        $result = $command->queryAll();
        return count($result);
    }

    public function ovView(){
        $overview = new CampaignOverviewClick();
        $overview->user_id = Yii::$app->user->id;
        $overview->campaign_id = $this->id;
        $overview->datetime = date('Y-m-d H:i:s');
        $overview->save(false);
    }

    public function ovBuy(){
        $overview = new CampaignOverviewBuy();
        $overview->user_id = Yii::$app->user->id;
        $overview->campaign_id = $this->id;
        $overview->datetime = date('Y-m-d H:i:s');
        $overview->save(false);
    }

    public function getOvBuys(){
        return $this->hasMany(CampaignOverviewBuy::className(), ['campaign_id'=>'id']);
    }

    public function getOvViews(){
        return $this->hasMany(CampaignOverviewClick::className(), ['campaign_id'=>'id']);
    }

    public function getOvViewCount(){
        return count($this->ovViews);
    }

    public function getOvBuyCount(){
        return count($this->ovBuys);
    }

    public function getChartBuys(){
        $connection = Yii::$app->getDb();
        $queryAll = "SELECT DATE(`datetime`) date, COUNT(id) total FROM campaign_overview_buy WHERE DATE(`datetime`) < DATE(NOW()) AND campaign_id = $this->id GROUP BY DATE(`datetime`)";
        $command = $connection->createCommand($queryAll);
        $result = $command->queryAll();
        return $result;
    }

    public function getChartClicks(){
        $connection = Yii::$app->getDb();
        $queryAll = "SELECT DATE(`datetime`) date, COUNT(id) total FROM campaign_overview_click WHERE DATE(`datetime`) < DATE(NOW()) AND campaign_id = $this->id GROUP BY DATE(`datetime`)";
        $command = $connection->createCommand($queryAll);
        $result = $command->queryAll();
        return $result;
    }
}
