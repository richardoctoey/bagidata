<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "clustering_detail_category".
 *
 * @property int $id
 * @property int $clustering_id
 * @property int $category_id
 */
class ClusteringDetailCategory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'clustering_detail_category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['clustering_id', 'category_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'clustering_id' => 'Clustering ID',
            'category_id' => 'Category ID',
        ];
    }
}
