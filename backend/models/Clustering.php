<?php

namespace backend\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "clustering".
 *
 * @property int $id
 * @property string $name
 * @property int $age_start
 * @property int $age_end
 * @property int $is_female
 * @property int $is_male
 *
 * @property AnalyticsClustering[] $analyticsClusterings
 * @property ClusteringDetailInterest[] $clusteringDetailInterests
 * @property ClusteringDetailLocation[] $clusteringDetailLocations
 * @property ClusteringDetailMarrital[] $clusteringDetailMarritals
 * @property ClusteringDetailReligion[] $clusteringDetailReligions
 */
class Clustering extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'clustering';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['age_start', 'age_end', 'is_female', 'is_male','incoming_end','incoming_start'], 'integer'],
            [['name'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'age_start' => 'Age Start',
            'age_end' => 'Age End',
            'is_female' => 'Is Female',
            'is_male' => 'Is Male',
            'incoming_end' => 'Incoming End',
            'incoming_start' => 'Incoming Start'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAnalyticsClusterings()
    {
        return $this->hasMany(AnalyticsClustering::className(), ['clustering_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClusteringDetailInterests()
    {
        return $this->hasMany(ClusteringDetailInterest::className(), ['clustering_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClusteringDetailLocations()
    {
        return $this->hasMany(ClusteringDetailLocation::className(), ['clustering_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClusteringDetailMarritals()
    {
        return $this->hasMany(ClusteringDetailMarrital::className(), ['clustering_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReligions()
    {
        return $this->hasMany(Religion::className(), ['id' => 'religion_id'])
            ->viaTable(ClusteringDetailReligion::tableName(), ['clustering_id' => 'id']);
    }

    public function getMarritals()
    {
        return $this->hasMany(Marrital::className(), ['id' => 'marrital_id'])
            ->viaTable(ClusteringDetailMarrital::tableName(), ['clustering_id' => 'id']);
    }

    public function getLocations()
    {
        return $this->hasMany(Location::className(), ['id' => 'location_id'])
            ->viaTable(ClusteringDetailLocation::tableName(), ['clustering_id' => 'id']);
    }

    public function getInterests()
    {
        $connection = Yii::$app->getDb();
        $command = $connection->createCommand("select interest as id, interest as name from clustering_detail_interest");
        $result = $command->queryAll();
        return ArrayHelper::map($result, 'id', 'name');
    }

    public function getCategories()
    {
        return $this->hasMany(Category::className(), ['id' => 'category_id'])
            ->viaTable(ClusteringDetailCategory::tableName(), ['clustering_id' => 'id']);
    }

    public function process($data){
        ClusteringDetailInterest::deleteAll(['clustering_id' => $this->id]);
        if(isset($data['Clustering']['interests'])) {
            foreach ($data['Clustering']['interests'] as $m) {
                $objInterest = new ClusteringDetailInterest();
                $objInterest->clustering_id = $this->id;
                $objInterest->interest = $m;
                $objInterest->save(false);
            }
        }

        ClusteringDetailLocation::deleteAll(['clustering_id' => $this->id]);
        if(isset($data['Clustering']['locations'])) {
            foreach ($data['Clustering']['locations'] as $m) {
                $objLocation = new ClusteringDetailLocation();
                $objLocation->clustering_id = $this->id;
                $objLocation->location_id = $m;
                $objLocation->save(false);
            }
        }

        ClusteringDetailReligion::deleteAll(['clustering_id' => $this->id]);
        if(isset($data['Clustering']['religions'])) {
            foreach ($data['Clustering']['religions'] as $m) {
                $objReligion = new ClusteringDetailReligion();
                $objReligion->clustering_id = $this->id;
                $objReligion->religion_id = $m;
                $objReligion->save(false);
            }
        }

        ClusteringDetailMarrital::deleteAll(['clustering_id'=>$this->id]);
        if($data['Clustering']['marritals']){
            foreach($data['Clustering']['marritals'] as $m){
                $objMarrital = new ClusteringDetailMarrital();
                $objMarrital->clustering_id =  $this->id;
                $objMarrital->marrital_id = $m;
                $objMarrital->save(false);
            }
        }

        ClusteringDetailCategory::deleteAll(['clustering_id'=>$this->id]);
        if($data['Clustering']['categories']){
            foreach($data['Clustering']['categories'] as $m){
                $objCategory = new ClusteringDetailCategory();
                $objCategory->clustering_id =  $this->id;
                $objCategory->category_id = $m;
                $objCategory->save(false);
            }
        }
    }

    public function getTotalMale(){
        $baseQuery = Campaign::getTotalReachPeople([$this->id],true);
        $connection = Yii::$app->getDb();
        $queryAll = "SELECT COALESCE(COUNT(user_id),0) FROM profile WHERE user_id IN ($baseQuery) AND gender='m'";
        $command = $connection->createCommand($queryAll);
        $result = $command->queryScalar();
        return $result;
    }

    public function getTotalFemale(){
        $baseQuery = Campaign::getTotalReachPeople([$this->id],true);
        $connection = Yii::$app->getDb();
        $queryAll = "SELECT COALESCE(COUNT(user_id),0) FROM profile WHERE user_id IN ($baseQuery) AND gender='f'";
        $command = $connection->createCommand($queryAll);
        $result = $command->queryScalar();
        return $result;
    }
}
