<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "point_transaction".
 *
 * @property int $id
 * @property int $user_id
 * @property int $point_get
 * @property int $previous_balance
 * @property string $comment
 * @property string $timestamp
 * @property string $debet_credit
 * @property string $transaction_type
 */
class PointTransaction extends \yii\db\ActiveRecord
{
    const TRANSACTION = 0;
    const ADJUSTMENT = 1;
    const WITHDRAW = 2;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'point_transaction';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id','approved','approve_time'],'safe'],
            [['user_id', 'point_get', 'previous_balance','transaction_type'], 'integer'],
            [['timestamp'], 'safe'],
            [['comment','debet_credit'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'point_get' => 'Point Get',
            'previous_balance' => 'Previous Balance',
            'comment' => 'Comment',
            'timestamp' => 'Timestamp',
            'debet_credit' => 'Debet Credit',
            'transaction_type' => 'Transaction Type'
        ];
    }

    public static function myBalance($userId){
        $connection = Yii::$app->getDb();
        $command = $connection->createCommand("
            SELECT coalesce(SUM(point_get),0) as total_balance
              FROM point_transaction
              WHERE user_id = :user_id
            GROUP BY user_id
        ",[':user_id'=>$userId]);

        $result = $command->queryOne();
        if(isset($result['total_balance'])){
            return (int) $result['total_balance'];
        }else{
            return 0;
        }
    }

    public static function increaseBalance($userId, $point, $comment, $creditOrDebit, $transactionType=0){
        return self::modifyBalance($userId, $point, $comment, $creditOrDebit, $transactionType);
    }

    public static function reduceBalance($userId, $point, $comment, $creditOrDebit, $transactionType=0){
        return self::modifyBalance($userId, $point*-1, $comment, $creditOrDebit, $transactionType);
    }

    public static function modifyBalance($userId, $point, $comment, $creditOrDebit, $transactionType=0){
        $walletTransaction =  new PointTransaction();
        $walletTransaction->user_id = $userId;
        $walletTransaction->point_get = $point;
        $walletTransaction->comment = $comment;
        $walletTransaction->debet_credit = $creditOrDebit;
        $walletTransaction->previous_balance = self::myBalance($userId);
        $walletTransaction->timestamp = date('Y-m-d H:i:s');
        $walletTransaction->transaction_type = $transactionType;
        if($walletTransaction->save(false)){return true;}
        return false;
    }
}
