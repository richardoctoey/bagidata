<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "campaign_clustering".
 *
 * @property int $id
 * @property int $campaign_id
 * @property int $clustering_id
 */
class CampaignClustering extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'campaign_clustering';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['campaign_id', 'clustering_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'campaign_id' => 'Campaign ID',
            'clustering_id' => 'Clustering ID',
        ];
    }
}
