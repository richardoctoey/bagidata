<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Receipt;

/**
 * ReceiptSearch represents the model behind the search form of `backend\models\Receipt`.
 */
class ReceiptSearch extends Receipt
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'total_point', 'approved'], 'integer'],
            [['receipt_1', 'receipt_2', 'receipt_3', 'receipt_4', 'receipt_5', 'receipt_6', 'receipt_7', 'receipt_8', 'receipt_9', 'receipt_10'], 'safe'],
            [['total_price'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Receipt::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'total_point' => $this->total_point,
            'total_price' => $this->total_price,
            'approved' => $this->approved,
        ]);

        $query->andFilterWhere(['like', 'receipt_1', $this->receipt_1])
            ->andFilterWhere(['like', 'receipt_2', $this->receipt_2])
            ->andFilterWhere(['like', 'receipt_3', $this->receipt_3])
            ->andFilterWhere(['like', 'receipt_4', $this->receipt_4])
            ->andFilterWhere(['like', 'receipt_5', $this->receipt_5])
            ->andFilterWhere(['like', 'receipt_6', $this->receipt_6])
            ->andFilterWhere(['like', 'receipt_7', $this->receipt_7])
            ->andFilterWhere(['like', 'receipt_8', $this->receipt_8])
            ->andFilterWhere(['like', 'receipt_9', $this->receipt_9])
            ->andFilterWhere(['like', 'receipt_10', $this->receipt_10]);

        return $dataProvider;
    }

    public function listUnapproved($params)
    {
        $query = Receipt::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'total_point' => $this->total_point,
            'total_price' => $this->total_price,
        ]);

        $query->andWhere([
            'approved' => null,
        ]);

        $query->andFilterWhere(['like', 'receipt_1', $this->receipt_1])
            ->andFilterWhere(['like', 'receipt_2', $this->receipt_2])
            ->andFilterWhere(['like', 'receipt_3', $this->receipt_3])
            ->andFilterWhere(['like', 'receipt_4', $this->receipt_4])
            ->andFilterWhere(['like', 'receipt_5', $this->receipt_5])
            ->andFilterWhere(['like', 'receipt_6', $this->receipt_6])
            ->andFilterWhere(['like', 'receipt_7', $this->receipt_7])
            ->andFilterWhere(['like', 'receipt_8', $this->receipt_8])
            ->andFilterWhere(['like', 'receipt_9', $this->receipt_9])
            ->andFilterWhere(['like', 'receipt_10', $this->receipt_10]);

        return $dataProvider;
    }
}
