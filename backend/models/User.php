<?php
/**
 * Created by PhpStorm.
 * User: Acer
 * Date: 17/04/2018
 * Time: 08.45
 */

namespace backend\models;


class User extends \dektrium\user\models\User
{
    public function getBankAccounts(){
        return $this->hasMany(BankAccount::className(), ['user_id' => 'id']);
    }
}