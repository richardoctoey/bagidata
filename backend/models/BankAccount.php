<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "bank_account".
 *
 * @property int $id
 * @property int $user_id
 * @property int $bank_id
 * @property string $account_name
 * @property string $account_no
 *
 * @property User $user
 */
class BankAccount extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'bank_account';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id'], 'safe'],
            [['user_id', 'bank_id'], 'integer'],
            [['account_name', 'account_no'], 'string', 'max' => 100],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'bank_id' => 'Bank ID',
            'account_name' => 'Account Name',
            'account_no' => 'Account No',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function getBank(){
        return $this->hasOne(Bank::className(), ['id' => 'bank_id']);
    }

    public static function bankAccounts($uid){
        $results = self::find()->where(['user_id'=>$uid])->all();
        return $results;
    }
}
