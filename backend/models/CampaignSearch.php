<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Campaign;

/**
 * CampaignSearch represents the model behind the search form of `backend\models\Campaign`.
 */
class CampaignSearch extends Campaign
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'is_sms', 'is_apps', 'is_email'], 'integer'],
            [['judul', 'description', 'date_start', 'date_end', 'picture_2', 'picture_3', 'picture_4', 'picture_1'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Campaign::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'date_start' => $this->date_start,
            'date_end' => $this->date_end,
            'is_sms' => $this->is_sms,
            'is_apps' => $this->is_apps,
            'is_email' => $this->is_email,
        ]);

        $query->andFilterWhere(['like', 'judul', $this->judul])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'picture_2', $this->picture_2])
            ->andFilterWhere(['like', 'picture_3', $this->picture_3])
            ->andFilterWhere(['like', 'picture_4', $this->picture_4])
            ->andFilterWhere(['like', 'picture_1', $this->picture_1]);

        return $dataProvider;
    }
}
