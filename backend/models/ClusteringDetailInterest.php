<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "clustering_detail_interest".
 *
 * @property int $id
 * @property int $clustering_id
 * @property string $interest
 *
 * @property Clustering $clustering
 * @property ClusteringDetailLocation[] $ids
 * @property ClusteringDetailMarrital[] $ids0
 * @property ClusteringDetailReligion[] $ids1
 * @property Clustering $clustering0
 */
class ClusteringDetailInterest extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'clustering_detail_interest';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['clustering_id'], 'integer'],
            [['interest'], 'string', 'max' => 100],
            [['clustering_id'], 'exist', 'skipOnError' => true, 'targetClass' => Clustering::className(), 'targetAttribute' => ['clustering_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'clustering_id' => 'Clustering ID',
            'interest' => 'Interest',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClustering()
    {
        return $this->hasOne(Clustering::className(), ['id' => 'clustering_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIds()
    {
        return $this->hasMany(ClusteringDetailLocation::className(), ['clustering_id' => 'id'])->viaTable('clustering', ['id' => 'clustering_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIds0()
    {
        return $this->hasMany(ClusteringDetailMarrital::className(), ['clustering_id' => 'id'])->viaTable('clustering', ['id' => 'clustering_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIds1()
    {
        return $this->hasMany(ClusteringDetailReligion::className(), ['clustering_id' => 'id'])->viaTable('clustering', ['id' => 'clustering_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClustering0()
    {
        return $this->hasOne(Clustering::className(), ['id' => 'clustering_id']);
    }

    public static function getAllInterestGrouppedBy(){
        $connection = Yii::$app->getDb();
        $command = $connection->createCommand("select items as id, items as name FROM receipt_details a GROUP BY a.items");
        $result = $command->queryAll();
        return $result;
    }
}
