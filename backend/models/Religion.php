<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "religion".
 *
 * @property int $id
 * @property string $religion_name
 */
class Religion extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'religion';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['religion_name'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'religion_name' => 'Religion Name',
        ];
    }
}
