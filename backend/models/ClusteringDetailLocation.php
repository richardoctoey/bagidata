<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "clustering_detail_location".
 *
 * @property int $id
 * @property int $clustering_id
 * @property int $location_id
 *
 * @property Clustering $clustering
 */
class ClusteringDetailLocation extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'clustering_detail_location';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['clustering_id', 'location_id'], 'integer'],
            [['clustering_id'], 'exist', 'skipOnError' => true, 'targetClass' => Clustering::className(), 'targetAttribute' => ['clustering_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'clustering_id' => 'Clustering ID',
            'location_id' => 'Location ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClustering()
    {
        return $this->hasOne(Clustering::className(), ['id' => 'clustering_id']);
    }
}
