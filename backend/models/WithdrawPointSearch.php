<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\WithdrawPoint;

/**
 * WithdrawPointSearch represents the model behind the search form of `backend\models\WithdrawPoint`.
 */
class WithdrawPointSearch extends WithdrawPoint
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'request', 'balance_on_request', 'approved'], 'integer'],
            [['request_time', 'approve_time', 'comment'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $statusApproved=null)
    {
        $query = WithdrawPoint::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'request' => $this->request,
            'balance_on_request' => $this->balance_on_request,
            'approved' => $this->approved,
            'request_time' => $this->request_time,
            'approve_time' => $this->approve_time,
        ]);

        if($statusApproved!==null){
            $query->andWhere([
                'approved' => $statusApproved
            ]);
        }

        $query->andFilterWhere(['like', 'comment', $this->comment]);

        return $dataProvider;
    }
}
