<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\WalletTransaction;

/**
 * WalletTransactionSearch represents the model behind the search form of `backend\models\WalletTransaction`.
 */
class WalletTransactionSearch extends WalletTransaction
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'point_get', 'previous_point', 'transaction_type'], 'integer'],
            [['comment', 'debet_credit', 'timestamp'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = WalletTransaction::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'point_get' => $this->point_get,
            'previous_point' => $this->previous_point,
            'transaction_type' => $this->transaction_type,
            'timestamp' => $this->timestamp,
        ]);

        $query->andFilterWhere(['like', 'comment', $this->comment])
            ->andFilterWhere(['like', 'debet_credit', $this->debet_credit]);

        return $dataProvider;
    }


    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function searchDebetCredit($params)
    {
        $query = ViewDebetCredit::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => Yii::$app->user->id,
            'point_get' => $this->point_get,
            'previous_point' => $this->previous_point,
            'transaction_type' => $this->transaction_type,
            'timestamp' => $this->timestamp,
        ]);

        $query->andFilterWhere(['like', 'comment', $this->comment])
            ->andFilterWhere(['like', 'debet_credit', $this->debet_credit]);

        $query->orderBy('timestamp ASC');

        return $dataProvider;
    }

    public function totalDebitCredit($userId){
        $connection = Yii::$app->getDb();
        $command = $connection->createCommand("
            SELECT coalesce(SUM(IF(debet_credit = 'd', point_get,0)),0) as total_debit, coalesce(SUM(IF(debet_credit = 'c', point_get,0)),0) as total_credit
              FROM view_debet_credit
              WHERE user_id = :user_id
            GROUP BY user_id
        ",[':user_id'=>$userId]);

        $result = $command->queryOne();
        return $result;
    }
}
