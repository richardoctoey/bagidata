<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "receipt_details".
 *
 * @property int $id
 * @property int $receipt_id
 * @property string $items
 * @property double $price
 * @property int $total_item
 * @property int $category_id
 *
 * @property Receipt $receipt
 * @property Receipt $receipt0
 */
class ReceiptDetails extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'receipt_details';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['receipt_id', 'total_item','category_id'], 'integer'],
            [['price'], 'number'],
            [['items'], 'string', 'max' => 100],
            [['receipt_id'], 'exist', 'skipOnError' => true, 'targetClass' => Receipt::className(), 'targetAttribute' => ['receipt_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'receipt_id' => 'Receipt ID',
            'items' => 'Items',
            'price' => 'Price',
            'total_item' => 'Total Item',
            'category_id' => 'Kategori Item'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReceipt()
    {
        return $this->hasOne(Receipt::className(), ['id' => 'receipt_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReceipt0()
    {
        return $this->hasOne(Receipt::className(), ['id' => 'receipt_id']);
    }

    public function getCategory(){
        return $this->hasOne(Receipt::className(), ['id' => 'category_id']);
    }

    public static function createMultiple($modelClass, $multipleModels = [])
    {
        $model    = new $modelClass;
        $formName = $model->formName();
        $post     = Yii::$app->request->post($formName);
        $models   = [];

        if (! empty($multipleModels)) {
            $keys = array_keys(ArrayHelper::map($multipleModels, 'id', 'id'));
            $multipleModels = array_combine($keys, $multipleModels);
        }

        if ($post && is_array($post)) {
            foreach ($post as $i => $item) {
                if (isset($item['id']) && !empty($item['id']) && isset($multipleModels[$item['id']])) {
                    $models[] = $multipleModels[$item['id']];
                } else {
                    $models[] = new $modelClass;
                }
            }
        }

        unset($model, $formName, $post);

        return $models;
    }
}
