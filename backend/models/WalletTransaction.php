<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "wallet_transaction".
 *
 * @property string $id
 * @property int $user_id
 * @property int $point_get
 * @property int $previous_point
 * @property string $comment
 * @property int $transaction_type 0 = transaction/redeem 1 = adjustment by admin 2 = withdraw
 * @property string $debet_credit
 * @property string $timestamp
 */
class WalletTransaction extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'wallet_transaction';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'point_get', 'previous_point', 'transaction_type'], 'integer'],
            [['timestamp'], 'safe'],
            [['comment'], 'string', 'max' => 50],
            [['debet_credit'], 'string', 'max' => 1],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'point_get' => 'Point Get',
            'previous_point' => 'Previous Point',
            'comment' => 'Comment',
            'transaction_type' => 'Transaction Type',
            'debet_credit' => 'Debet Credit',
            'timestamp' => 'Timestamp',
        ];
    }

    public static function myBalance($userId){
        $connection = Yii::$app->getDb();
        $command = $connection->createCommand("
            SELECT coalesce(SUM(point_get),0) as total_balance
              FROM view_debet_credit
              WHERE user_id = :user_id
            GROUP BY user_id
        ",[':user_id'=>$userId]);

        $result = $command->queryOne();
        if(isset($result['total_balance'])){
            return (int) $result['total_balance'];
        }else{
            return 0;
        }
    }

    public static function increaseBalance($userId, $point, $comment, $creditOrDebit){
        return self::modifyBalance($userId, $point, $comment, $creditOrDebit);
    }

    public static function reduceBalance($userId, $point, $comment, $creditOrDebit){
        return self::modifyBalance($userId, $point*-1, $comment, $creditOrDebit);
    }
    
    public static function modifyBalance($userId, $point, $comment, $creditOrDebit){
        $walletTransaction =  new WalletTransaction();
        $walletTransaction->user_id = $userId;
        $walletTransaction->point_get = $point;
        $walletTransaction->comment = $comment;
        $walletTransaction->debet_credit = $creditOrDebit;
        $walletTransaction->previous_point = self::myBalance($userId);
        $walletTransaction->timestamp = date('Y-m-d H:i:s');
        if($walletTransaction->save(false)){return true;}
        return false;
    }
}
