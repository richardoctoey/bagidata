<?php

namespace backend\controllers;

use Yii;
use backend\models\WithdrawPoint;
use backend\models\WithdrawPointSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\models\PointTransaction;
use backend\models\PointTransactionSearch;

/**
 * WithdrawPointController implements the CRUD actions for WithdrawPoint model.
 */
class WithdrawPointController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all WithdrawPoint models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new WithdrawPointSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionListUnapproved()
    {
        $searchModel = new WithdrawPointSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, 0);

        return $this->render('list-unapproved', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionListApproved()
    {
        $searchModel = new WithdrawPointSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, 1);

        return $this->render('list-approved', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionProcess($id){
        $model = $this->findModel($id);
        $model->approve_time = date('Y-m-d H:i:s');
        if($model->load(Yii::$app->request->post()) && $model->save()){
            if($model->approved == 1){
                PointTransaction::reduceBalance($model->user_id, $model->request, $model->comment, 'c', PointTransaction::WITHDRAW);
            }
            return $this->redirect(['withdraw-point/list-unapproved']);
        }
        return $this->render('process', [
            'model' => $model,
        ]);
    }

    public function actionListRejected()
    {
        $searchModel = new WithdrawPointSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, -1);

        return $this->render('list-rejected', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single WithdrawPoint model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new WithdrawPoint model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        if(!count(Yii::$app->user->identity->bankAccounts)){
            Yii::$app->session->setFlash('isi-bankacc', 'Anda harus mengisi informasi bank terlebih dahulu');
            return $this->redirect(['bank-account/create']);
        }

        $balance = PointTransaction::myBalance(Yii::$app->user->id);

        $model = new WithdrawPoint();
        $model->approved = 0;
        $model->balance_on_request = $balance;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('waiting-approval', "Status penarikan anda akan segera dikabarkan lewat SMS");
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
            'balance' => $balance,
        ]);
    }

    /**
     * Updates an existing WithdrawPoint model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing WithdrawPoint model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the WithdrawPoint model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return WithdrawPoint the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = WithdrawPoint::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
