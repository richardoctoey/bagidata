<?php
/**
 * Created by PhpStorm.
 * User: Acer
 * Date: 16/04/2018
 * Time: 16.26
 */

namespace backend\controllers;

use backend\models\ProfileBagiData;
use dektrium\user\controllers\AdminController;
use yii\helpers\Url;
use dektrium\user\models\Profile;

class ProfileController extends AdminController
{

    public function behaviors()
    {
        return [];
    }

    public function actionUpdateProfile($id)
    {
        Url::remember('', 'actions-redirect');
        $user    = $this->findModel($id);
        $profile = $user->profile;
        if ($profile == null) {
            $profile = \Yii::createObject(ProfileBagiData::className());
            $profile->link('user', $user);
        }
        $event = $this->getProfileEvent($profile);
        $this->performAjaxValidation($profile);
        $this->trigger(self::EVENT_BEFORE_PROFILE_UPDATE, $event);

        if ($profile->load(\Yii::$app->request->post()) && $profile->save()) {
            \Yii::$app->getSession()->setFlash('success', \Yii::t('user', 'Profile details have been updated'));
            $this->trigger(self::EVENT_AFTER_PROFILE_UPDATE, $event);
            return $this->refresh();
        }
        return $this->render('/user/admin/_profile', [
            'user'    => $user,
            'profile' => $profile,
            'percentage' => $profile->getFilledData()
        ]);
    }

}